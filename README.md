# Spark Quick Start

## Spark is built with Scala

- Compiled language that runs on the JVM
- uses type inference. This means all variables has a definite type - (but you don't neccsarily need to declare them)
- supports interop with Java and Clojure
- supports functional and OOP style programming

## Install scala

```brew install scala```

## Scala Basics

To start playing with scala, fire up the scala REPL by typing "scala" at the command line.

### Variables can be declared with using the "var" or "val" keywords. "val" variables cannot be reassigned, "var" variables can.

```scala
var count = 1
count += 1  // no problem

val age = 39 
age = age + 1 //error: reassignment to val
```

### Expression based rather than statement based. This means that every piece of code returns a value

```scala
val count = 3
val evenOrOdd = if (count % 2 == 0) { "even" } else  { "odd" } // equals to the string "odd"
```

Even methods that perform actions rather than computing results return values:

```scala
scala> val result = println("hello")
hello
result: Unit = ()  //Unit is a "placeholder" type
```

### supports functional methods like map, filter, reduce as well as pattern matching

This is important to know about because you can use these functions locally on collection, or on spark data structures to run parallel distributed jobs. Its essentially the same code with the same syntax.

```scala
def isEven(x:Int) = x % 2 == 0
val nums = Range(1,10)
val evens = nums.filter( x => isEven(x) ) /// 2,4,6,8
val totalEvens = evens.reduce(_ + _) // 20

//or more succinctly:
val totalEvens = Range(1,10)
  .filter(isEven)
  .reduce( _ + _)  // "_" is a place holder for the collection elements being reduced
```

### failure and empty states can be represented with Option types, and are handled safely with for and flatMap

Perhaps we have a list of people, and we wish to transform the data:
```scala
val candidates = List( ("Hillary Clinton", "68"), ("Bernie Sanders", "74"), ("Donald Trump", ""), ("Ted Cruz","45"), ("Marco Rubio", "44"))

//naively, we might apply a mapping:
val newCandidates = candidates.map{  cn => (cn._1 , cn._2.toInt) };
//but encounter an error: java.lang.NumberFormatException: For input string: ""
```

so we can use a type to represent the success or failure of our operation:

```scala
  def parseCandidate(name:String, age:String) : Option[(String,Int)]  = {
    try {
      Some( (name, age.toInt)  ) //return the candidate with the parsed age
    } catch  {
      case _ : Throwable => None // returns and empty Option
    }
  }

```

now if we perform our mapping, we get:

```scala 
candidates.map( cn => parseCandidate(cn._1,cn._2))
//returns List(Some((Hillary Clinton,68)), Some((Bernie Sanders,74)), None, Some((Ted Cruz,45)), Some((Marco Rubio,44)))
```

Which is definitely an improvement because the offending candidate has been excised.
But a problem remains: we don't want "Some" objects - we want the names and ages.
Furthermore, its bothersome to filter out the empty "None" object. Fortunately we have the "flatMap" function which will releive us of this burden:

```scala
candidates.flatMap( cn => parseCandidate(cn._1,cn._2))
//returns  List((Hillary Clinton,68), (Bernie Sanders,74), (Ted Cruz,45), (Marco Rubio,44))
```

The 'flatMap' function is [useful in many otherways](http://twitter.github.io/effectivescala/#Functional%20programming-%60flatMap%60) , but I won't go into that right now.

## Traditional Flow Control

Scala also supports the traditional flow control mechanisms like for, while and do-while:

```scala
for( a <- 1 to 10){
  println( "a:" + a );
}

var c = 10
while (c>0) {
  println(c);
  c = c-1
}

c = 10
do  {
  println(c);
  c = c-1
} while (c>0)
```

Additionally, Scala supports pattern matching, which is like a more flexible case statement:

```scala
abstract class ClaimType
case class Professional() extends ClaimType
case class Institutional() extends ClaimType

val transform : Claimtype = Professional()

val transformDescription = transform match {
  case Professional() => "P"
  case Institutional() => "I"
}
```

The nice thing about Scala pattern matching is that it can be used to deconstruct your data types on the fly.
So suppose we had a transformation type that was specific to a diagnosis:

```scala
sealed abstract class ClaimType
case class Professional( diagnosis:String ) extends ClaimType
case class Institutional( diagnosis:String ) extends ClaimType

val transform : ClaimType = Professional("M10042")

val transformDescription = transform match {
  case Professional("M10042") => "P_gout"
  case Professional(_) => "P"
  case Institutional(_) => "I"
}
```

This concision allows you to specify an otherwise complicated case analyis and can greatly simplify code. But a further benefit is that it allows totality checking.
If the Institutional case clause was not specified, the compiler can automatically determine the the statement does not exhaustively 
match all the possiblities, and raise a compile time warning. To test this out, comment out the Institutional case and try pasting it into the REPL.

## Using The Spark Shell / Scala REPL

To experiment with Spark, [download a prebuilt package](https://spark.apache.org/downloads.html)
As of June 2017, Spark 2.1.1 is the latest version supported by [Amazon EMR](https://aws.amazon.com/elasticmapreduce/), so that's what we'll use.

Once you've unpacked it, go to the spark directory and start the shell:

```
~/project $ cd spark
~/project/spark $ ./bin/spark-shell
...
Welcome to
      ____              __
     / __/__  ___ _____/ /__
    _\ \/ _ \/ _ `/ __/  '_/
   /___/ .__/\_,_/_/ /_/\_\   version 2.1.1
      /_/
scala>
```

First, we're going to load some JSON data. Spark supports [many data formats](https://spark.apache.org/docs/latest/sql-data-sources.html) such as json, text, parquet, ORC.

```scala
scala> val iceCream = spark.read.json("/Users/jsnavely/project/scala_spark_quick_start/icecream.json")
iceCream: org.apache.spark.sql.DataFrame = [Name: string, Preference: string]
```

In our example, we read from a single file that contains JSON element per line. In production, you could use this 
same command to read petabytes of data from thousands of files stored on HDFS or S3.

Spark builds a dataframe, and infers a schema using the keys in the JSON. We're now ready to do some aggregate opperations.

To find the most popular flavors:

```scala
scala> iceCream.groupBy("preference").count().show()
+------------+-----+
|  preference|count|
+------------+-----+
|  Strawberry|    2|
|     Vanilla|    2|
|Butter Pecan|    1|
|   Chocolate|    2|
+------------+-----+

```

Now let's compare people's ice cream preferences with dinner orders.

```scala

scala> val dinner = spark.read.json("/Users/jsnavely/project/scala_spark_quickstart/dinner.json")
dinner: org.apache.spark.sql.DataFrame = [dinner1: string, dinner2: string, name: string]


scala> val dinner_and_dessert  = dinner.join(iceCream, usingColumn="name")
dinner_and_dessert: org.apache.spark.sql.DataFrame = [name: string, dinner1: string, dinner2: string, preference: string]

```

Notice here that (A) the result returned immediately and (B) we don't see any results yet. This is because spark uses a [lazy evaluation](https://en.wikipedia.org/wiki/Lazy_evaluation) strategy.
This means that no work is performed until its forced. Forcing events would be writing data out to disk or collecting them in memory to display.  Calling show will force the action.

```scala
scala> dinner_and_dessert.show()
+-------+--------+-----------+------------+
|   name| dinner1|    dinner2|  preference|
+-------+--------+-----------+------------+
|    Jim|   steak|       fish|   Chocolate|
|    Jim|   steak|       fish|Butter Pecan|
|    Jim|broccoli|   potatoes|   Chocolate|
|    Jim|broccoli|   potatoes|Butter Pecan|
|Patrick| chicken|       fish|     Vanilla|
|   Kyle|   steak|  dumplings|   Chocolate|
|   Kyle|   steak|  dumplings|  Strawberry|
|   Kyle|    fish|green beans|   Chocolate|
|   Kyle|    fish|green beans|  Strawberry|
|   Greg|   sushi|    burrito|     Vanilla|
|    Tim| chicken|    falafel|  Strawberry|
+-------+--------+-----------+------------+
```

An important consequence of lazy evaluation is that multiple transformations on a RDD, DataFrame or Dataset can be performed in a single step through 
an optimization called [loop-fusion](https://en.wikipedia.org/wiki/Loop_fusion)

## Do it in SQL

```scala
scala> dinner.createTempView("dinner")
scala> spark.sql("select count(*) from dinner where  dinner1 ='fish' or dinner2 ='fish'").show
scala> spark.sql("select name from dinner where  dinner1 ='fish' or dinner2 ='fish'")
```
